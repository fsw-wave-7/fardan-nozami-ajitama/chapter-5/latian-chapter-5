const fs = require("fs");
const createPerson = (person) => {
  fs.writeFileSync("./person.json", JSON.stringify(person));
  return person;
};

const Nozami = createPerson({
  name: "Nozami",
  address: "Bantul",
  age: 25,
});
