// const os = require("os");
// const fs = require("fs");

// // core module untuk membaca memori
// console.log(`Free Memory:`, os.freemem());

// // membaca isi file
// const isi = fs.readFileSync("./text.txt", "utf-8");
// console.log(isi);

// // buat file baru
// const nulis = fs.writeFileSync("./text.txt", "tulisan tmbahan");

// const nozami = require("./person.json");

// console.log(nozami);

// HTTP server respon HTML
// const http = require("http");
// const fs = require("fs");

// function onRequest(req, res) {
//   res.writeHead(200, { "Content-Type": "text/html" });
//   fs.readFile("index.html", null, (error, data) => {
//     if (error) {
//       res.writeHead(404);
//       res.write("file not Found");
//     } else {
//       res.write(data);
//     }
//     res.end();
//   });
// }

// http.createServer(onRequest).listen(3000);

// HTTP server respon JSON
// const http = require("http");
// const fs = require("fs");

// function onRequest(req, res) {
//   res.writeHead(200, { "Content-Type": "application/json" });
//   const data = {
//     name: "Fardan",
//     age: 24,
//   };
//   res.end(JSON.stringify(data));
// }

// http.createServer(onRequest).listen(3000);

// express
const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send("Hello Wolrd");
});

app.listen(port, () =>
  console.log(`Server berhasil dijalankan pada port ${port}`)
);
